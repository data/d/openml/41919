# OpenML dataset: CPMP-2015-runtime-classification

https://www.openml.org/d/41919

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

source: An Algorithm Selection Benchmark for the Container Pre-Marshalling Problem (CPMP)
authors: K. Tierney and Y. Malitsky (features) / K. Tierney and D. Pacino and S. Voss (algorithms)
translator in coseal format: K. Tierney

This is an extension of the 2013 premarshalling dataset that includes more features and a set of test instances. 

There are three sets of features:

feature_values.arff contains the full set of features from iteration 2 of our latent feature analysis (LFA) process (see paper)
feature_values_itr1.arff contains only the features after iteration 1 of LFA
feature_values_orig.arff containers the features used in PREMARHSALLING-ASTAR-2013

We also provide test data with an identical naming scheme (see _test). 

The features for the pre-marshalling problem are all extremely easy and fast to
compute, thus the feature_costs.arff file has been omitted, as it would be time
0 for every feature (regardless of using original, iteration 1 or iteration 2
features).

The feature computation code is available at https://bitbucket.org/eusorpb/cpmp-as

Note: previously the scenario was called PREMARSHALLING-ASTAR-2015. To save same space, we renamed the scenario.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41919) of an [OpenML dataset](https://www.openml.org/d/41919). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41919/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41919/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41919/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

